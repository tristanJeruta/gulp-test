const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const concat = require("gulp-concat");
const terser = require("gulp-terser");
const sourcemaps = require("gulp-sourcemaps");
const postcss = require("gulp-postcss");
const cssnano = require("cssnano");
const autoprefixer = require("autoprefixer");
const purgecss = require("gulp-purgecss");
var rollup = require("gulp-better-rollup");
var babel = require("rollup-plugin-babel");
var panini = require("panini");
const { src, series, parallel, dest, watch } = require("gulp");

const jsPath = "src/assets/js/**/*.js";
const cssPath = "src/assets/css/**/*.css";

function copyHtml() {
  panini.refresh();
  return src("src/**/*.html")
    .pipe(
      panini({
        root: "src/pages/",
        layouts: "src/layouts/",
        partials: "src/partials/",
        data: "src/data/",
      })
    )
    .pipe(gulp.dest("dist"));
}

function imgTask() {
  return src("src/assets/images/*")
    .pipe(imagemin())
    .pipe(gulp.dest("dist/assets/images"));
}

function jsTask() {
  return src(jsPath)
    .pipe(sourcemaps.init())
    .pipe(rollup({ plugins: [babel()] }, { format: "cjs" }))
    .pipe(concat("all.js"))
    .pipe(terser())
    .pipe(sourcemaps.write("."))
    .pipe(dest("dist/assets/js"));
}

function cssTask() {
  return src(cssPath)
    .pipe(
      purgecss({
        content: ["src/**/*.html"],
      })
    )
    .pipe(sourcemaps.init())
    .pipe(concat("style.css"))
    .pipe(postcss([autoprefixer(), cssnano()])) //not all plugins work with postcss only the ones mentioned in their documentation
    .pipe(sourcemaps.write("."))
    .pipe(dest("dist/assets/css"));
}

function watchTask() {
  watch([cssPath, jsPath], { interval: 1000 }, parallel(cssTask, jsTask));
  watch(["./src/{layouts,partials,data,pages}/**/*"], copyHtml);
}

exports.cssTask = cssTask;
exports.jsTask = jsTask;
exports.imgTask = imgTask;
exports.copyHtml = copyHtml;
exports.default = series(
  parallel(copyHtml, imgTask, jsTask, cssTask),
  watchTask
);
